<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Archive Template
 *
 *
 * @file           archive.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.1
 * @filesource     wp-content/themes/responsive/archive.php
 * @link           http://codex.wordpress.org/Theme_Development#Archive_.28archive.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>

<ul class="menu dani">
	<li class="active"><a id="2015-10-16" class="dan" href="#">Петак 16.10.</a></li>
    <li><a id="2015-10-19" class="dan" href="#">Понедељак 19.10.</a></li>
    <li><a id="2015-10-20" class="dan" href="#">Уторак 20.10.</a></li>
    <li><a id="2015-10-21" class="dan" href="#">Среда 21.10.</a></li>
    <li><a id="2015-10-22" class="dan" href="#">Четвртак 22.10.</a></li>
    <li><a id="2015-10-23" class="dan" href="#">Петак 23.10.</a></li>
    <li><a id="2015-10-24" class="dan" href="#">Субота 24.10.</a></li>
    <li><a id="2015-10-25" class="dan" href="#">Недеља 25.10.</a></li>
</ul>

<div id="content-archive" class="<?php echo esc_attr( implode( ' ', responsive_get_content_classes() ) ); ?>">

<?php
	$args = array( 'post_type' => 'np_dogadjaji', 'posts_per_page' => '-1', 'meta_key' => 'np_dogadjaji_datum_od', 'meta_value' => '2015-10-16' );
	$loop = new WP_Query( $args );
	if($loop->have_posts()) :
        while ( $loop->have_posts() ) : $loop->the_post();
            get_template_part('template-parts/loop',get_post_type());
    	endwhile;
    else:
        echo "<p>За жељени дан не постоји ниједан креиран догађај.</p>\r\n";
    endif;


    ?>

</div><!-- end of #content-archive -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
