<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Footer Template
 *
 *
 * @file           footer.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.2
 * @filesource     wp-content/themes/responsive/footer.php
 * @link           http://codex.wordpress.org/Theme_Development#Footer_.28footer.php.29
 * @since          available since Release 1.0
 */

/*
 * Globalize Theme options
 */
global $responsive_options;
$responsive_options = responsive_get_options();
?>
<?php responsive_wrapper_bottom(); // after wrapper content hook ?>
</div><!-- end of #wrapper -->
<?php responsive_wrapper_end(); // after wrapper hook ?>
</div><!-- end of #container -->
<?php responsive_container_end(); // after container hook ?>

<div id="footer" class="clearfix container">
	<?php responsive_footer_top(); ?>

		<?php $upload_dir = wp_upload_dir(); ?>
	<!-- <img class='img-bcg' src='<?php //echo $upload_dir['baseurl'] . '/2015/05/footer-img.jpg'; ?>' /> -->
	<div class='footer-bcg clearfix'>
		<div class="footer-wrapper">
            <div class="grid col-460">&nbsp;</div>
            <div class="grid col-460 fit">
                <div class="grid col-460 center">
                    <p>
                        <a class='btn np-blue' href='<?php echo site_url() . '/kontakt'; ?>'>Контакт</a>
                    </p>
                    <p>
                        <a class='link' target="_blank" href='mailto:office@nedeljaparlamentarizma.rs?subject=Kontakt sa sajta <?php echo site_url(); ?>'>Пошаљите нам е-маил</a>
                    </p>
                </div>
                <div class="grid col-460 fit center">
                    <p>
                        <a class='btn np-blue' href=''; ?>Пратите нас</a>
                    </p>
                    <p>
                        <a href='https://www.facebook.com/nedeljaparlamentarizma?fref=ts' target='_blank'><img alt='' src='<?php echo $upload_dir['baseurl'] . '/2015/10/facebook.png'; ?>' /></a><a href='https://twitter.com/ImateRec	' target='_blank'><img alt='' src='<?php echo $upload_dir['baseurl'] . '/2015/10/twitter.png'; ?>' /></a>
                    </p>
                </div>
            </div>

		</div><!-- end #footer-wrapper -->
	</div> <!-- end .footer-bcg -->

	<div class=' partners'>
		<div class='grid col-220'>
			<a href='http://www.parlament.gov.rs/' target='_blank'><img alt='' src='<?php echo $upload_dir['baseurl'] . '/2015/05/narodna-skupstina-republike-srbije.png'; ?>' /></a>
		</div>
		<div class='grid col-220'>
			<a href='http://otvoreniparlament.rs/' target='_blank'><img alt='' src='<?php echo $upload_dir['baseurl'] . '/2015/05/otvoreni-parlament.png'; ?>' /></a>
		</div>
		<div class='grid col-220'>
			<a href='http://www.usaid.gov/where-we-work/europe-and-eurasia/serbia' target='_blank'><img alt='' src='<?php echo $upload_dir['baseurl'] . '/2015/05/usaid.png'; ?>' /></a>
		</div>
		<div class='grid col-220'>
			<a href='https://www.gov.uk/government/world/organisations/british-embassy-belgrade.sr' target='_blank'><img alt='' src='<?php echo $upload_dir['baseurl'] . '/2015/05/british-embassy-belgrade.png'; ?>' /></a>
		</div>
		<div class='grid col-220'>
			<a href='https://www.eda.admin.ch/countries/serbia/en/home/representations/cooperation-office.html' target='_blank'><img alt='' src='<?php echo $upload_dir['baseurl'] . '/2015/10/sdc_logo.png'; ?>' /></a>
		</div>
		<div class='grid col-220 fit' style="width: 65px">
			<a href='http://www.rs.undp.org/content/serbia/en/home.html' target='_blank'><img alt='' src='<?php echo $upload_dir['baseurl'] . '/2015/10/undp_logo.png'; ?>' /></a>
		</div>
	</div>

	<div class='footer-wrapper center'>
		<p class='clear'><?php _e('Израда овог сајта омогућена је уз подршку америчког народа путем Америчке агенције за међународни развој (УСАИД), Британске Амбасаде у Београду, Програма Уједињених нација за развој и Швајцарске агенције за развој и сарадњу. За садржај сајта одговоран је аутор и он не мора нужно одражавати ставове УСАИД-а, Владе Сједињених Америчких Држава, Британске амбасаде у Београду, Програм Уједињених нација за развој и Швајцарске агенције за развој и сарадњу'); ?></p>		                           
	</div>
	

	<?php responsive_footer_bottom(); ?>
</div><!-- end #footer -->

<?php responsive_footer_after(); ?>

<?php wp_footer(); ?>
</body>
</html>