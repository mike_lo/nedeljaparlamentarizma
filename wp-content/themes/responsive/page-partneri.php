<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Pages Template
 *
 *
 * @file           page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>

	<div class='grid col-940' style="display: inline-flex;">

	<div class="col-620">
		<?php if ( have_posts() ) : ?>

			<?php while( have_posts() ) : the_post(); ?>

				<?php //get_template_part( 'loop-header', get_post_type() ); ?>

				<?php responsive_entry_before(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php responsive_entry_top(); ?>

					<?php get_template_part( 'post-meta', get_post_type() ); ?>

					<div class="post-entry">
						<?php the_content( __( 'Прочитајте више &#8250;', 'responsive' ) ); ?>
						<?php wp_link_pages( array( 'before' => '<div class="pagination">' . __( 'Pages:', 'responsive' ), 'after' => '</div>' ) ); ?>
					</div><!-- end of .post-entry -->

					<?php responsive_entry_bottom(); ?>
				</div><!-- end of #post-<?php the_ID(); ?> -->
				<?php responsive_entry_after(); ?>

			<?php
			endwhile;

		endif; 	?>
		</div>

		<style type="text/css">
			.relative{position: relative;}
			.cent-pos{
				top: 50%;
			    left: 50%;
			    transform: translate(-50%, -50%);
			    -webkit-transform: translate(-50%, -50%);
			    padding: 15px 25px;
			}
		</style>

	<?php responsive_widgets_before(); // above widgets container hook ?>
	<div id="widgets" class="grid col-300 fit" >
		<?php responsive_widgets(); // above widgets hook ?>

		<?php if ( !dynamic_sidebar( 'right-sidebar' ) ) : ?>
			<div class="widget-wrapper relative ">
				<?php  


				$ideje_tekst = get_field('ideja_tekst'); 
				$ideje_slika = get_field('ideja_slika'); 


				if($ideje_slika)
					$ideje_slika = $ideje_slika['url'];
				if(empty($ideje_tekst)) 
					$ideje_tekst = "";

							
					
				?>
				
				<img src="<?php echo $ideje_slika;?>" class="img-responsive dark-img" >
				<div class="info-box-text cent-pos">
		            <?php echo $ideje_tekst; ?>
		        </div>
				<!-- get_stylesheet_directory_uri()/img/Box1.1.jpg -->
			<!-- 	<div class="widget-title"><h3><?php _e( 'In Archive', 'responsive' ); ?></h3></div>
			<ul>
				<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
			</ul> -->

			</div><!-- end of .widget-wrapper -->


			<div class="widget-wrapper" style="text-align: center;">
			
				<h2>Пратите нас</h2>
                <a href="https://www.facebook.com/nedeljaparlamentarizma?fref=ts" target="_blank"><img alt="" src="http://www.nedeljaparlamentarizma.rs/wp-content/uploads/2015/10/facebook.png"></a>
                <a href="https://twitter.com/ImateRec&#9;" target="_blank"><img alt="" src="http://www.nedeljaparlamentarizma.rs/wp-content/uploads/2015/10/twitter.png"></a>
            
			<!-- 	<div class="widget-title"><h3><?php _e( 'In Archive', 'responsive' ); ?></h3></div>
			<ul>
				<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
			</ul> -->

			</div><!-- end of .widget-wrapper -->
		<?php endif; //end of right-sidebar ?>

		<?php responsive_widgets_end(); // after widgets hook ?>
	</div><!-- end of #widgets -->
	<?php responsive_widgets_after(); // after widgets container hook ?>	
	</div> <!-- end .grid .col-940 -->

    <?php
        $boks_1 = get_field( 'boks_1_p', 48 );
        $boks_2 = get_field( 'boks_2_p', 48 );
        $boks_3 = get_field( 'boks_3_p', 48 );

        $boks_1_slika = get_field('boks_1_slika_p', 48);
        $boks_2_slika = get_field('boks_2_slika_p', 48);
    ?>

	<div class="grid col-460 info-box <?php if(!$boks_1_slika) echo 'info-box-no-img'; ?>">
        <?php if($boks_1_slika): ?>
            <img src="<?php echo $boks_1_slika; ?>" class="img-responsive" />
            <img class="innerCont" src="<?php echo get_template_directory_uri() . '/img/box-senka.png'; ?>" alt="" />
        <?php endif; ?>
        <div class="info-box-text">
            <?php echo $boks_1; ?>
        </div>
	</div>
	<div class="grid col-460 info-box fit <?php if(!$boks_2_slika) echo 'info-box-no-img'; ?>">
        <?php if($boks_2_slika): ?>
            <img src="<?php echo $boks_2_slika; ?>" class="img-responsive" />
            <img class="innerCont" src="<?php echo get_template_directory_uri() . '/img/box-senka.png'; ?>" alt="" />
        <?php endif; ?>
        <div class="info-box-text">
            <?php echo $boks_2; ?>
        </div>
	</div>

    <div class="clear">
        <?php echo $boks_3; ?>
    </div>

<?php get_footer(); ?>
