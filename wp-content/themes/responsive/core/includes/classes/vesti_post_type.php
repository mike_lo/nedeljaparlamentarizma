<?php
/**
 * Created by PhpStorm.
 * User: Damjan
 * Date: 17.6.15
 * Time: 13:21
 */

class Vesti_post_type {

    function aktiviraj()
    {
        register_post_type( 'np_vesti',
            array(
                'labels' => array(
                    'name' => __( 'Вести' ),
                    'singular_name' => __( 'Вести' ),
                    'add_new' => __('Додај нову'),
                    'edit_item' => __('Измени вест'),
                    'add_new_item' => __('Додај нову вест')
                ),
                'menu_icon' => 'dashicons-admin-page',
                'public' => true,
                'has_archive' => true,
                'taxonomies' => array( 'category' , 'post_tag' ),
                'rewrite' => array('slug' => 'vesti'),
                'supports'          => array('title', 'thumbnail', 'editor', 'excerpt')
            )
        );
    }

    function kolone($columns)
    {
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => "Vest",
            "author" => "Autor",
            "date" => "Datum izmene"
        );

        return $columns;
    }


}