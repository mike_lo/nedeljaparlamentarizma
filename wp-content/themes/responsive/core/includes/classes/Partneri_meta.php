<?php


class Partneri_meta {

	public function __construct()
	{
		add_action( 'show_user_profile', array($this, 'np_show_extra_profile_fields') );
		add_action( 'edit_user_profile', array($this, 'np_show_extra_profile_fields') );

        add_action( 'personal_options_update', array($this, 'np_save_extra_profile_fields') );
        add_action( 'edit_user_profile_update', array($this, 'np_save_extra_profile_fields') );
    }

	function np_show_extra_profile_fields($user)
	{
        if ( isset( $user->roles ) && is_array( $user->roles ) )
        {
            if (in_array('np_partner_role', $user->roles))
            {
                $html = "";
                $html .= "<h3>Dodatne informacije</h3>";



                $html .= '<table class="form-table">';

//                if ( isset( $user->roles ) && is_array( $user->roles ) ) {
                    //check for admin
                    if(current_user_can('administrator'))
                    {
//                        if (in_array('np_partner_role', $user->roles))
//                        {
                            $odobren_meta = get_the_author_meta( 'odobren', $user->ID );
                            $odobren_checkbox = ($odobren_meta == '1') ? 'checked="checked"' : '';
                            $html .= "<tr>\r\n";
                            $html .= "<th><label for='odobren'>Одобрен</label></th>\r\n";
                            $html .= "<td>\r\n";
                            $html .= "<input type='checkbox' name='odobren' id='odobren' value='1' $odobren_checkbox /><br />\r\n";
                            // $html .= "<span class='description'>Molim Vas unesite ime organizacije</span>\r\n";
                            $html .= "</td>\r\n";
                            $html .= "</tr>\r\n";
//                        }
                    }
//                }

                // Ime organizacije
                $html .= "<tr>\r\n";
                $html .= "<th><label for='organizacija'>Ime organizacije</label></th>\r\n";
                $html .= "<td>\r\n";
                $html .= "<input type='text' name='organizacija' id='organizacija' value='". esc_attr( get_the_author_meta( 'organizacija', $user->ID ) ) ."' class='regular-text' /><br />\r\n";
                // $html .= "<span class='description'>Molim Vas unesite ime organizacije</span>\r\n";
                $html .= "</td>\r\n";
                $html .= "</tr>\r\n";

                // Opis delatnosti
                $html .= "<tr>\r\n";
                $html .= "<th><label for='delatnost'>Opis delatnosti</label></th>\r\n";
                $html .= "<td>\r\n";
                $html .= "<input type='text' name='delatnost' id='delatnost' value='". esc_attr( get_the_author_meta( 'delatnost', $user->ID ) ) ."' class='regular-text' /><br />\r\n";
                // $html .= "<span class='description'>Molim Vas unesite ime organizacije</span>\r\n";
                $html .= "</td>\r\n";
                $html .= "</tr>\r\n";

                // Adresa
                $html .= "<tr>\r\n";
                $html .= "<th><label for='adresa'>Adresa</label></th>\r\n";
                $html .= "<td>\r\n";
                $html .= "<input type='text' name='adresa' id='adresa' value='". esc_attr( get_the_author_meta( 'adresa', $user->ID ) ) ."' class='regular-text' /><br />\r\n";
                // $html .= "<span class='description'>Молим Вас унесите Adresa</span>\r\n";
                $html .= "</td>\r\n";
                $html .= "</tr>\r\n";

                // Kontakt osoba
                $html .= "<tr>\r\n";
                $html .= "<th><label for='kontakt_osoba'>Kontakt osoba</label></th>\r\n";
                $html .= "<td>\r\n";
                $html .= "<input type='text' name='kontakt_osoba' id='kontakt_osoba' value='". esc_attr( get_the_author_meta( 'kontakt_osoba', $user->ID ) ) ."' class='regular-text' /><br />\r\n";
                // $html .= "<span class='description'>Молим Вас унесите име организације</span>\r\n";
                $html .= "</td>\r\n";
                $html .= "</tr>\r\n";

                // Telefon
                $html .= "<tr>\r\n";
                $html .= "<th><label for='telefon'>Telefon</label></th>\r\n";
                $html .= "<td>\r\n";
                $html .= "<input type='text' name='telefon' id='telefon' value='". esc_attr( get_the_author_meta( 'telefon', $user->ID ) ) ."' class='regular-text' /><br />\r\n";
                // $html .= "<span class='description'>Молим Вас унесите име организације</span>\r\n";
                $html .= "</td>\r\n";
                $html .= "</tr>\r\n";

                // Website
                //		$html .= "<tr>\r\n";
                //		$html .= "<th><label for='website'>Internet stranica</label></th>\r\n";
                //		$html .= "<td>\r\n";
                //		$html .= "<input type='text' name='website' id='website' value='". esc_attr( get_the_author_meta( 'website', $user->ID ) ) ."' class='regular-text' /><br />\r\n";
                //		// $html .= "<span class='description'>Молим Вас унесите име организације</span>\r\n";
                //		$html .= "</td>\r\n";
                //		$html .= "</tr>\r\n";

                // Twitter
                $html .= "<tr>\r\n";
                $html .= "<th><label for='twitter'>Twitter</label></th>\r\n";
                $html .= "<td>\r\n";
                $html .= "<input type='text' name='twitter' id='twitter' value='". esc_attr( get_the_author_meta( 'twitter', $user->ID ) ) ."' class='regular-text' /><br />\r\n";
                // $html .= "<span class='description'>Молим Вас унесите име организације</span>\r\n";
                $html .= "</td>\r\n";
                $html .= "</tr>\r\n";

                // Opis
                $html .= "<tr>\r\n";
                $html .= "<th><label for='opis'>Kratak opis događaja ili aktivnosti</label></th>\r\n";
                $html .= "<td>\r\n";
                $html .= "<textarea name='opis' id='opis' row='5' cols='30'>". esc_attr( get_the_author_meta( 'opis', $user->ID ) ) ."</textarea><br />\r\n";
                // $html .= "<span class='description'>Молим Вас унесите име организације</span>\r\n";
                $html .= "</td>\r\n";
                $html .= "</tr>\r\n";

                // Ucesnici
                $html .= "<tr>\r\n";
                $html .= "<th><label for='ucesnici'>Ko bi bili učesnici događaja ili aktivnosti</label></th>\r\n";
                $html .= "<td>\r\n";
                $html .= "<textarea name='ucesnici' id='ucesnici' row='5' cols='30'>". esc_attr( get_the_author_meta( 'ucesnici', $user->ID ) ) ."</textarea><br />\r\n";
                // $html .= "<span class='description'>Молим Вас унесите име организације</span>\r\n";
                $html .= "</td>\r\n";
                $html .= "</tr>\r\n";

                $html .= "</table>\r\n";

                echo $html;
            }
        }


	}

    function np_save_extra_profile_fields( $user_id ) {
		 if ( !current_user_can( 'edit_user', $user_id ) )
		 	return false;

        $odobren_meta_field = sanitize_text_field($_POST['odobren']);
        $odobren_meta = empty($odobren_meta_field) ? '0' : $odobren_meta_field;

        $odobren_prev_value = get_the_author_meta('odobren', $user_id);

        if($odobren_prev_value == '0' && $odobren_meta == '1')
        {
            // samo u ovom slucaju saljemo mail
            $user = get_userdata( $user_id );

            $to = $user->data->user_email;

            $subject = 'Aktivacija naloga na sajtu ' . site_url();

            $message = ""; // HTML body
            $message .= (!empty($user->data->display_name)) ? "<p>Poštovani-a ".$user->data->display_name .",</p>\r\n" : "<p>Poštovani-a,</p>\r\n";
            $message .= "<p>Čestitamo! Postali ste zvanični partner \"Nedelje parlamentarizma\". Vaš predlog za učešće u \"Nedelji parlamentarizma\" je odobren. Sledeći korak jeste da popunite formular koji se nalazi na ovom linku: <a href='https://docs.google.com/forms/d/1rEDQi0rI6hrcM8Vb6vXk42rXh5pU-Ijz-wA4Wygfdgg/viewform' target='_blank'>https://docs.google.com/forms/d/1rEDQi0rI6hrcM8Vb6vXk42rXh5pU-Ijz-wA4Wygfdgg/viewform</a> kako bismo informacije o Vašem događaju ili aktivnosti predstavili na sajtu \"Nedelje parlamentarizma\".</p>\r\n";
            $message .= "<p>Za sve dodatne informacije, možete nas kontaktirati putem e-maila office@nedeljaparlamentarizma.rs ili na telefon 063 591 268.</p>\r\n";
            $message .= "<p>Srdačan pozdrav, </p>\r\n";
            $message .= "<p>Tim Nedelje parlamentarizma.</p>";

            $headers = array("Content-type: text/html; charset=UTF-8");

            @wp_mail($to, $subject, $message, $headers);
        }

        update_user_meta( $user_id, 'odobren', $odobren_meta );
        update_user_meta( $user_id, 'organizacija', sanitize_text_field($_POST['organizacija']) );
		update_user_meta( $user_id, 'delatnost', sanitize_text_field($_POST['delatnost']) );
		update_user_meta( $user_id, 'adresa', sanitize_text_field($_POST['adresa']) );
		update_user_meta( $user_id, 'kontakt_osoba', sanitize_text_field($_POST['kontakt_osoba']) );
		update_user_meta( $user_id, 'telefon', sanitize_text_field($_POST['telefon']) );
		update_user_meta( $user_id, 'website', sanitize_text_field($_POST['website']) );
		update_user_meta( $user_id, 'twitter', sanitize_text_field($_POST['twitter']) );
		update_user_meta( $user_id, 'opis', sanitize_text_field($_POST['opis']) );
		update_user_meta( $user_id, 'ucesnici', sanitize_text_field($_POST['ucesnici']) );
	}
}