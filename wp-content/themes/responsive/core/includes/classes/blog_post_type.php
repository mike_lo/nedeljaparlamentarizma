<?php
/**
 * Created by PhpStorm.
 * User: Damjan
 * Date: 17.6.15
 * Time: 13:50
 */

class Blog_post_type {

    function aktiviraj()
    {
        register_post_type( 'np_blog',
            array(
                'labels' => array(
                    'name' => __( 'Блог' ),
                    'singular_name' => __( 'Блог' ),
                    'add_new' => __('Додај нови'),
                    'edit_item' => __('Измени блог'),
                    'add_new_item' => __('Додај нови блог')
                ),
                'menu_icon' => 'dashicons-welcome-write-blog',
                'public' => true,
                'has_archive' => true,
                'rewrite' => array('slug' => 'blog'),
                'supports'          => array('title', 'thumbnail', 'editor', 'comments', 'excerpt')
            )
        );
    }

    function kolone($columns)
    {
        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => "Blog",
            "author" => "Autor",
            "date" => "Datum izmene"
        );

        return $columns;
    }
}