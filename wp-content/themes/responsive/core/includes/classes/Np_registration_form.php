<?php


class Np_registration_form {
	
	private $ime;
	private $prezime;
	private $email;
	private $username;
	private $password;
	private $role;
	private $organizacija;
	private $delatnost;
	private $adresa;
	private $kontakt_osoba;
	private $telefon;
	private $website;
	private $twitter;
	private $opis;
	private $ucesnici;
	private $uslovi;

	private static $wp_error; // Will hold global variable safely

	function __construct()
	{
	    add_shortcode('np_registration_form', array($this, 'shortcode'));
	    // add_action('wp_enqueue_scripts', array($this, 'flat_ui_kit'));
	}

	function np_registration_form_fields()
	{
		$form = "";

		$form = "<div class='grid col-620 info-box'>\r\n";
		$form .= "<form class='cmxform' method='post' action='".esc_url($_SERVER['REQUEST_URI'])."'>\r\n";
		// $form .= "<fieldset>\r\n";
		// $form .= "<legend>Регистрација</legend>\r\n";
		
		// Osnovni podaci za registraciju

		// Ime
		$form .= "<p>\r\n";
		$form .= "<label for='ime'>Име</label>\r\n";
		$form .= "<input class='form-control' id='ime' name='reg_ime' type='text' placeholder='Име' />\r\n";
		$form .= "</p>\r\n";

		// Prezime
		$form .= "<p>\r\n";
		$form .= "<label for='prezime'>Презиме</label>\r\n";
		$form .= "<input class='form-control' id='prezime' name='reg_prezime' type='text' placeholder='Презиме' />\r\n";
		$form .= "</p>\r\n";

		// E-mail
		$form .= "<p>\r\n";
		$form .= "<label for='email'>Е-мејл *</label>\r\n";
		$form .= "<input id='email' name='reg_email' type='text' placeholder='Е-мејл' class='required form-control email' />\r\n";
		$form .= "</p>\r\n";

		// Username
		/*$form .= "<p>\r\n";
		$form .= "<label for='username'>Корисничко име *</label>\r\n";
		$form .= "<input id='username' name='reg_username' type='text' placeholder='Корисничко име' class='required form-control' />\r\n";
		$form .= "</p>\r\n";*/

		// Password
//		$form .= "<p>\r\n";
//		$form .= "<label for='password'>Лозинка *</label>\r\n";
//		$form .= "<input id='password' name='reg_password' type='password' placeholder='Лозинка' class='required form-control' />\r\n";
//		$form .= "</p>\r\n";


		// Dodatni podaci

		// Ime organizacije
		$form .= "<p>\r\n";
		$form .= "<label for='organizacija'>Име организације *</label>\r\n";
		$form .= "<input id='organizacija' name='reg_organizacija' type='text' placeholder='Име организације' class='required form-control' />\r\n";
		$form .= "</p>\r\n";

		// Opis delatnosti
		$form .= "<p>\r\n";
		$form .= "<label for='delatnost'>Опис делатности *</label>\r\n";
		$form .= "<input id='delatnost' name='reg_delatnost' type='text' placeholder='Опис делатности' class='required form-control' />\r\n";
		$form .= "</p>\r\n";

		// Adresa
		$form .= "<p>\r\n";
		$form .= "<label for='adresa'>Адреса *</label>\r\n";
		$form .= "<input id='adresa' name='reg_adresa' type='text' placeholder='Адреса' class='required form-control' />\r\n";
		$form .= "</p>\r\n";

		// Kontakt osoba ??
		$form .= "<p>\r\n";
		$form .= "<label for='kontakt_osoba'>Контакт особа *</label>\r\n";
		$form .= "<input id='kontakt_osoba' name='reg_kontakt_osoba' type='text' placeholder='Контакт особа' class='required form-control' />\r\n";
		$form .= "</p>\r\n";

		// Telefon
		$form .= "<p>\r\n";
		$form .= "<label for='telefon'>Телефон *</label>\r\n";
		$form .= "<input id='telefon' name='reg_telefon' type='text' placeholder='Телефон' class='required form-control' />\r\n";
		$form .= "</p>\r\n";

		// Website
		$form .= "<p>\r\n";
		$form .= "<label for='website'>Интернет страница</label>\r\n";
		$form .= "<input class='form-control' id='website' name='reg_website' type='text' placeholder='Интернет страница' />\r\n";
		$form .= "</p>\r\n";

		// Twitter
		$form .= "<p>\r\n";
		$form .= "<label for='twitter'>Твитер</label>\r\n";
		$form .= "<input class='form-control' id='twitter' name='reg_twitter' type='text' placeholder='Твитер' />\r\n";
		$form .= "</p>\r\n";

		// Opis dogadjaja
		$form .= "<p>\r\n";
		$form .= "<label for='opis'>Кратак опис догађаја или активности *</label>\r\n";
		$form .= "<textarea id='opis' name='reg_opis' class='required form-control' rows='5' ></textarea>\r\n";
		$form .= "</p>\r\n";

		// Ucesnici
		$form .= "<p>\r\n";
		$form .= "<label for='ucesnici'>Ко би били учесници догађаја или активности *</label>\r\n";
		$form .= "<input id='ucesnici' name='reg_ucesnici' type='text' placeholder='Ко би били учесници догађаја или активности' class='required form-control' />\r\n";
		$form .= "</p>\r\n";

		// Услови
		$form .= "<p>\r\n";
		$form .= "<label for='uslovi'>\r\n";
		$form .= "<input id='uslovi' name='reg_uslovi' type='checkbox' class='required' />\r\n";
		$form .= "Слажем се са <a href='". site_url().'/uslovi-ucesca-u-nedelji-parlamentarizma/' ."' target='_blank'>условима учешћа у Недељи парламентаризма</a></label>\r\n";
		$form .= "</p>\r\n";

		

		$form .= "<p>\r\n";
		$form .= "<input type='submit' value='Региструј се' name='reg_submit' class='btn np-blue' />\r\n";
		$form .= "</p>\r\n";

		// $form .= "</fieldset>\r\n";

		$form .= "</form>\r\n";
		$form .= "</div>\r\n";

		echo $form;
	}

	function validation()
	{
		if(empty($this->username))
		{
			return new WP_Error('field', 'Морате попунити сва обавезна поља (*).');
		}

		if(empty($this->email))
		{
			return new WP_Error('field', 'Морате попунити сва обавезна поља (*).');
		}

		if(!is_email($this->email))
		{
			return new WP_Error('field', 'Морате унети исправну е-мејл адресу.');
		}

//		if(empty($this->password))
//		{
//			return new WP_Error('field', 'Морате попунити сва обавезна поља (*).');
//		}

		if(empty($this->organizacija))
		{
			return new WP_Error('field', 'Морате попунити сва обавезна поља (*).');
		}

		if(empty($this->delatnost))
		{
			return new WP_Error('field', 'Морате попунити сва обавезна поља (*).');
		}

		if(empty($this->adresa))
		{
			return new WP_Error('field', 'Морате попунити сва обавезна поља (*).');
		}

		if(empty($this->kontakt_osoba))
		{
			return new WP_Error('field', 'Морате попунити сва обавезна поља (*).');
		}

		if(empty($this->telefon))
		{
			return new WP_Error('field', 'Морате попунити сва обавезна поља (*).');
		}

		if(empty($this->opis))
		{
			return new WP_Error('field', 'Морате попунити сва обавезна поља (*).');
		}

		if(empty($this->ucesnici))
		{
			return new WP_Error('field', 'Морате попунити сва обавезна поља (*).');
		}

		if(empty($this->uslovi))
		{
			return new WP_Error('field', 'Морате попунити сва обавезна поља (*).');
		}

	}

	function registration()
	{
		$userdata = array(
			'user_login' => esc_attr($this->username),
	        'user_email' => esc_attr($this->email),
	        'user_pass' => esc_attr($this->password),
	        'first_name' => esc_attr($this->ime),
	        'last_name' => esc_attr($this->prezime),
            'user_url'  => esc_attr($this->website),
	        'role' => 'np_partner_role',
		);

		if (is_wp_error($this->validation())) 
		{
	        echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';
	        echo '<strong>' . $this->validation()->get_error_message() . '</strong>';
	        echo '</div>';
	    }
	    else
	    {
	    	$register_user = wp_insert_user($userdata);
	        if (!is_wp_error($register_user)) 
	        {
	        	$to = $this->email;
	        	$subject = "Registracija partnera na sajtu " . site_url();
	        	
	        	$message = ""; // HTML body
	        	$message .= (!empty($this->ime) || !empty($this->prezime)) ? "<p>Poštovani-a $this->ime $this->prezime,</p>\r\n" : "<p>Poštovani-a,</p>\r\n";
	        	$message .= "<p>Hvala Vam što ste izrazili interesovanje da postanete zvanični partner \"Nedelje parlamentarizma\". Organizacioni tim \"Nedelje parlamentarizma\" će u najkraćem roku razmotriti Vaš predlog. Ukoliko se Vaš događaj ili aktivnost tematski uklapa i ispunjava uslove \"Nedelje parlamentarizma\", dobićete e-mail sa uputstvom za naredne korake.</p>\r\n";
	        	$message .= "<p>Srdačan pozdrav, </p>\r\n";
	        	$message .= "<p>Tim Nedelje parlamentarizma.</p>";
	        	
	        	$headers = array("Content-type: text/html; charset=UTF-8");
	        	
	        	@wp_mail($to, $subject, $message, $headers);

	            echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';
	            echo '<strong>Успешно сте се регистровали. Проверите Ваш е-мејл за даље инструкције.</strong>';
	            echo '</div>';

	            // Ubacujemo meta data
	            add_user_meta($register_user, 'organizacija', esc_attr($this->organizacija));
	            add_user_meta($register_user, 'delatnost', esc_attr($this->delatnost));
	            add_user_meta($register_user, 'adresa', esc_attr($this->adresa));
	            add_user_meta($register_user, 'kontakt_osoba', esc_attr($this->kontakt_osoba));
	            add_user_meta($register_user, 'telefon', esc_attr($this->telefon));
//	            add_user_meta($register_user, 'website', esc_attr($this->website));
	            add_user_meta($register_user, 'twitter', esc_attr($this->twitter));
	            add_user_meta($register_user, 'opis', esc_attr($this->opis));
	            add_user_meta($register_user, 'ucesnici', esc_attr($this->ucesnici));
	            add_user_meta($register_user, 'odobren', 0);
	        }
	        else 
	        {
	            echo '<div style="margin-bottom: 6px" class="btn btn-block btn-lg btn-danger">';
	            echo '<strong>' . __($register_user->get_error_message()) . '</strong>';
	            echo '</div>';
	            echo "<div class='clear'>&nbsp;</div>\r\n";
	        }
	    }
	}

	function shortcode()
    {
        ob_start();
 
        if ($_POST['reg_submit']) {
            $this->username = $_POST['reg_email'];
            $this->email = $_POST['reg_email'];
            $this->password = rand(1,6);
            $this->ime = $_POST['reg_ime'];
            $this->prezime = $_POST['reg_prezime'];
            $this->organizacija = $_POST['reg_organizacija'];
            $this->delatnost = $_POST['reg_delatnost'];
            $this->adresa = $_POST['reg_adresa'];
            $this->kontakt_osoba = $_POST['reg_kontakt_osoba'];
            $this->telefon = $_POST['reg_telefon'];
            $this->twitter = $_POST['reg_twitter'];
            $this->opis = $_POST['reg_opis'];
            $this->ucesnici = $_POST['reg_ucesnici'];
            $this->uslovi = $_POST['reg_uslovi'];
            $this->website = $_POST['reg_website'];

            // $this->validation();
            $this->registration();
        }
 
        $this->np_registration_form_fields();
        return ob_get_clean();
    }

	function np_errors()
	{
		return isset($this->wp_error) ? $this->wp_error : ($this->wp_error = new WP_Error(null, null, null));
	}

	

}