jQuery(document).ready(function($) {
	var options = $.datepicker.regional[ "sr-SR" ];
	options.dateFormat = "yy-mm-dd";
	options.changeMonth = true;
	options.changeYear = true;
	options.yearRange = "c-40:c+15";
	$(".tfdate").datepicker(options);

    // Time picker
    $('.tftime').timepicker({
        'timeFormat': 'H:i',
        'show2400': 'true'
    });
});