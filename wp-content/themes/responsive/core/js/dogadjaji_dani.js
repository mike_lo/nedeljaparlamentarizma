/**
 * Created by Damjan on 12.6.15.
 */

jQuery(document).ready(function(){
    jQuery('.dan').on('click', function(e){
        var a = jQuery(this);
        e.preventDefault();
        //jQuery.post(
        //    myAjax.ajaxurl,
        //    {
        //        'action': 'np_dogadjaji_dani',
        //        'data':   a.attr('id')
        //    },
        //    function(response){
        //        console.log(response);
        //    }
        //);

        jQuery.ajax({
            'url': myAjax.ajaxurl,
            'type': 'POST',
            'dataTYpe': 'html',
            'data': {'dan': a.attr('id'), 'action': 'np_dogadjaji_dani'},
            success: function(data, textStatus, jqXHR) {
                jQuery('#content-archive').html(data);
            },
            complete: function(jqXHR, textStatus) {
                jQuery('.dani li').removeClass('active');
                a.parent('li').addClass('active');
            }
        });
    });
});
