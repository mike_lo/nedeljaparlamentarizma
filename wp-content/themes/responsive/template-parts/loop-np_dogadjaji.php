<?php responsive_entry_before(); ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php responsive_entry_top(); ?>

    <?php get_template_part( 'post-meta', get_post_type() ); ?>

    <div class="post-entry">

        <?php the_excerpt(); ?>
        <?php wp_link_pages( array( 'before' => '<div class="pagination">' . __( 'Pages:', 'responsive' ), 'after' => '</div>' ) ); ?>
    </div><!-- end of .post-entry -->

    <?php //get_template_part( 'post-data', get_post_type() ); ?>

    <a class="more-link"  href="<?php the_permalink(); ?>">Сазнајте више ></a>

    <?php responsive_entry_bottom(); ?>
</div><!-- end of #post-<?php the_ID(); ?> -->
<?php responsive_entry_after(); ?>
<hr />