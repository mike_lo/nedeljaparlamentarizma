<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

$responsive_options = responsive_get_options();
//test for first install no database
$db = get_option( 'responsive_theme_options' );
//test if all options are empty so we can display default text if they are
$empty     = ( empty( $responsive_options['home_headline'] ) && empty( $responsive_options['home_subheadline'] ) && empty( $responsive_options['home_content_area'] ) ) ? false : true;
$emtpy_cta = ( empty( $responsive_options['cta_text'] ) ) ? false : true;

?>

    <?php
        $boks_1 = get_field('boks_1', 77);
        $boks_2 = get_field('boks_2', 77);
        $boks_3 = get_field('boks_3', 77) ;
        $boks_4 = get_field('boks_4', 77) ;
        $boks_4 = get_field('boks_5', 77) ;

        $boks_1_slika = get_field('boks_1_slika', 77);
        $boks_2_slika = get_field('boks_2_slika', 77);
        $boks_3_slika = get_field('boks_3_slika', 77);
        $boks_4_slika = get_field('boks_4_slika', 77);
        $boks_4_slika = get_field('boks_5_slika', 77);

//    var_dump($boks_1_slika);die;
    ?>
<!--<img src="--><?php //the_field('boks_2_slika'); ?><!--" alt="" />-->
	<div class="grid col-300 info-box <?php if(!$boks_1_slika) echo 'info-box-no-img'; ?>">
        <?php if($boks_1_slika): ?>
            <img src="<?php echo $boks_1_slika; ?>" class="img-responsive dark-img" />            
        <?php endif; ?>
        <div class="info-box-text">
            <?php echo $boks_1; ?>
        </div>
    </div>
	<div class="grid col-300 info-box <?php if(!$boks_2_slika) echo 'info-box-no-img'; ?>">
        <?php if($boks_2_slika): ?>
            <img src="<?php echo $boks_2_slika; ?>" class="img-responsive dark-img" />            
        <?php endif; ?>
        <div class="info-box-text">
            <?php echo $boks_2; ?>
        </div>
    </div>

    <div class="grid col-300 info-box <?php if(!$boks_3_slika) echo 'info-box-no-img'; ?>">
        <?php if($boks_3_slika): ?>
            <img src="<?php echo $boks_3_slika; ?>" class="img-responsive dark-img" />            
        <?php endif; ?>
        <div class="info-box-text">
            <?php echo $boks_3; ?>
        </div>
    </div>

    <div class="grid col-300 info-box <?php if(!$boks_4_slika) echo 'info-box-no-img'; ?>">
        <?php if($boks_4_slika): ?>
            <img src="<?php echo $boks_4_slika; ?>" class="img-responsive dark-img" />            
        <?php endif; ?>
        <div class="info-box-text">
            <?php echo $boks_4; ?>
        </div>
    </div>

     <div class="grid col-300 info-box <?php if(!$boks_5_slika); /*echo 'info-box-no-img';*/ ?>">
        <iframe width="300" height="300" src="https://www.youtube.com/embed/bdN_JK6bJQY" frameborder="0" allowfullscreen></iframe>

       <!--  <?php if($boks_5_slika): ?>
           <img src="<?php echo $boks_5_slika; ?>" class="img-responsive dark-img" />            
       <?php endif; ?>
       <div class="info-box-text">
           <?php echo $boks_5; ?>
       </div> -->
    </div>
    

	<div class="grid col-300 info-box  info-box-no-img">
		<a class="twitter-timeline" href="https://twitter.com/ImateRec" data-widget-id="654200860453548032">Tweets by @ImateRec</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</div>
