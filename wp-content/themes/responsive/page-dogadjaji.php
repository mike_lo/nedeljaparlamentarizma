<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Pages Template
 *
 *
 * @file           page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>

	<div class='grid col-940'>
		<?php if ( have_posts() ) : ?>

			<?php while( have_posts() ) : the_post(); ?>

				<?php //get_template_part( 'loop-header', get_post_type() ); ?>

				<?php responsive_entry_before(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php responsive_entry_top(); ?>

					<?php get_template_part( 'post-meta', get_post_type() ); ?>

					<div class="post-entry">
						<?php the_content( __( 'Read more &#8250;', 'responsive' ) ); ?>
						<?php wp_link_pages( array( 'before' => '<div class="pagination">' . __( 'Pages:', 'responsive' ), 'after' => '</div>' ) ); ?>
					</div><!-- end of .post-entry -->

					<?php responsive_entry_bottom(); ?>
				</div><!-- end of #post-<?php the_ID(); ?> -->
				<?php responsive_entry_after(); ?>

			<?php
			endwhile;

		endif; 	?>
	</div> <!-- end .grid .col-940 -->

	<div class="grid col-300 info-box">
		<h3>Преузмите Водич за партнере</h3>
		<div class='textwidget'>Сед ут перспициатис унде омнис исте натус еррор сит волуптатем аццусантиум долоремqуе лаудантиум, тотам рем апериам, еаqуе ипса qуае аб илло ...</div>
	</div>
	<div class="grid col-300 info-box">
		<h3>Постаните партнер - Региструјте се и пријавите догађај</h3>
		<div class='textwidget'>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo ...</div>
	</div>
	<div class="grid col-300 info-box fit">
		<h3>Реците нам више о свом дгађају</h3>
		<div class='textwidget'>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo ...</div>
	</div>

<?php get_footer(); ?>
