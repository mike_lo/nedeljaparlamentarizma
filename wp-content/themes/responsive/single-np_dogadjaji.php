<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Single Posts Template
 *
 *
 * @file           single.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/single.php
 * @link           http://codex.wordpress.org/Theme_Development#Single_Post_.28single.php.29
 * @since          available since Release 1.0
 */

get_header(); ?>

<div id="content" class="grid col-940">

    <?php get_template_part( 'loop-header', get_post_type() ); ?>

    <?php if ( have_posts() ) : ?>

        <?php while( have_posts() ) : the_post(); ?>

            <?php
            // Polja
            $id = get_the_ID();
            $naziv_organizacije = get_post_meta( $id, 'np_dogadjaji_naziv_organizacije', true );
//            $format = get_post_meta( $id, 'np_dogadjaji_format', true );
            $opis = get_post_meta( $id, 'np_dogadjaji_opis', true );

            $datum = get_post_meta( $id, 'np_dogadjaji_datum_od', true );
            $vreme_od = get_post_meta( $id, 'np_dogadjaji_vreme_od', true );
            $vreme_do = get_post_meta( $id, 'np_dogadjaji_vreme_do', true );



            $grad = get_post_meta( $id, 'np_dogadjaji_grad', true );
            $ulica_broj = get_post_meta( $id, 'np_dogadjaji_ulica_broj', true );
            $prostor = get_post_meta( $id, 'np_dogadjaji_prostor', true );

            $dodatne_informacije = get_post_meta( $id, 'np_dogadjaji_dodatne_informacije', true );

//            $kontakt_ime_prezime = get_post_meta( $id, 'np_dogadjaji_kontakt_ime_prezime', true );
//            $kontakt_email = get_post_meta( $id, 'np_dogadjaji_kontakt_email', true );
//            $kontakt_telefon = get_post_meta( $id, 'np_dogadjaji_kontakt_telefon', true );

//            $prijava_ucesnika = get_post_meta( $id, 'np_dogadjaji_prijava_ucesnika', true );
//            $prijava_ucesnika_da = ($prijava_ucesnika == '1') ? 'checked="checked"' : '';
//            $prijava_ucesnika_ne = ($prijava_ucesnika == '0') ? 'checked="checked"' : '';
//
//            $nacin_prijave = get_post_meta( $id, 'np_dogadjaji_nacin_prijave', true );
//            $broj_ucesnika = get_post_meta( $id, 'np_dogadjaji_broj_ucesnika', true );
//            $uticaj = get_post_meta( $id, 'np_dogadjaji_uticaj', true );

//            $logo = the_post_thumbnail();

//            var_dump($broj_ucesnika); die;


            ?>

            <?php responsive_entry_before(); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php responsive_entry_top(); ?>

                <h1 class="entry-title post-title"><?php the_title(); ?></h1>

                <div class="post-entry">

                    <?php


                    if ( has_post_thumbnail() ) :
                        the_post_thumbnail( 'full', array( 'class' => 'alignleft' ) );
                    endif;
                    echo "<div class='clear'></div>\r\n";
                    echo "<p><strong>Партнер: </strong>$naziv_organizacije</p>\r\n";
                    echo "<p><strong>Датум и време: </strong>".ucfirst(date_i18n( 'l, j. F Y.', strtotime($datum) ))." $vreme_od - $vreme_do</p>";
                    echo "<p><strong>Локација: </strong>$prostor, $ulica_broj, $grad</p>";
//                    echo "<h6>Опис догађаја</h6>";
                    echo "<div>" . $opis . "</div>";

                    echo '<p><strong>' . $dodatne_informacije . '</strong></p>';
//                    echo "<h6>Контакт</h6>";
//                    $kontakt = '';
//                    $kontakt .= !empty($kontakt_ime_prezime) ? $kontakt_ime_prezime : '';
//                    $kontakt .= !empty($kontakt_email) ? ', ' . $kontakt_email : '';
//                    $kontakt .= !empty($kontakt_telefon) ? ', ' . $kontakt_telefon : '';
//                    echo "<div>$kontakt</div>";
//                    echo "<h6>Додатне информације</h6>";
//                    echo ($prijava_ucesnika === '1') ? "<div>Пријава учесника: $nacin_prijave</div>" : "";
//                    echo !empty($broj_ucesnika) ? "<div>Број учесника: $broj_ucesnika" : "";
                    ?>

                    <?php wp_link_pages( array( 'before' => '<div class="pagination">' . __( 'Pages:', 'responsive' ), 'after' => '</div>' ) ); ?>
                </div><!-- end of .post-entry -->

                <?php //get_template_part( 'post-data', get_post_type() ); ?>

                <?php responsive_entry_bottom(); ?>
            </div><!-- end of #post-<?php the_ID(); ?> -->
            <?php responsive_entry_after(); ?>

            <?php responsive_comments_before(); ?>
            <?php comments_template( '', true ); ?>
            <?php responsive_comments_after(); ?>

        <?php
        endwhile;

        get_template_part( 'loop-nav', get_post_type() );

    else :

        get_template_part( 'loop-no-posts', get_post_type() );

    endif;
    ?>

</div><!-- end of #content -->

<?php get_footer(); ?>
