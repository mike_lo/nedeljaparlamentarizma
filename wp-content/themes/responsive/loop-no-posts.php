<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * No-Posts Loop Content Template-Part File
 *
 * @file           loop-no-posts.php
 * @package        Responsive
 * @author         Damjan Bursac
 * @copyright      2003 - 2015 GBI
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/loop-no-posts.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */

/**
 * If there are no posts in the loop,
 * display default content
 */
$title = ( is_search() ? sprintf( __( 'Нема резултата за Вашу претрагу: %s.', 'responsive' ), get_search_query() ) : __( '404 &#8212; Није пронађен ниједан чланак!', 'responsive' ) );
?>

	<h1 class="title-404"><?php echo $title; ?></h1>

	<h6><?php
		printf( __( 'Можете се врати на почетну страницу - %s', 'responsive' ),
				sprintf( '<a href="%1$s" title="%2$s">%3$s</a>',
						 esc_url( get_home_url() ),
						 esc_attr__( 'Почетна', 'responsive' ),
						 esc_attr__( '&larr; Home', 'responsive' )
				)
		);
		?></h6>