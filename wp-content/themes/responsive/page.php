<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Pages Template
 *
 *
 * @file           page.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/page.php
 * @link           http://codex.wordpress.org/Theme_Development#Pages_.28page.php.29
 * @since          available since Release 1.0
 */

    // Ako je submitovana kontakt forma salje mail

    $kontakt_submit = $_POST['kontakt_submit'];

    if(isset($kontakt_submit))
    {
        $ime = sanitize_text_field($_POST['ime']);
        $email = sanitize_text_field($_POST['email']);
        $poruka = sanitize_text_field($_POST['poruka']);

        if(empty($ime) || empty($email) || empty($poruka))
        {
            $kontakt_form_error = 'Morate popuniti sva obavezna polja.';
        }
        else
        {
            $to = 'office@nedeljaparlamentarizma.rs';
            $subject = 'Kontakt sa sajta ' . site_url();
            $message = '';
            $message .= '<p>Poštovani,</p>';
            $message .= '<p>Dobili ste novu poruku sa kontakt forme sajta ' . site_url() . '</p>';
            $message .= "<p>Ime: $ime <br/>";
            $message .= "E-mail: $email <br/>";
            $message .= "Poruka: $poruka</p>";
            $headers = array("Content-type: text/html; charset=UTF-8");

            @wp_mail($to, $subject,$message, $headers);
        }
    }

get_header(); ?>

	<div class='grid col-940'>
		<?php if ( have_posts() ) : ?>

			<?php while( have_posts() ) : the_post(); ?>

				<?php //get_template_part( 'loop-header', get_post_type() ); ?>

				<?php responsive_entry_before(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php responsive_entry_top(); ?>
                    <?php echo isset($kontakt_form_error) ? "<span style='color:#ff0000;'>$kontakt_form_error</span>" : ''; ?>
					<?php get_template_part( 'post-meta', get_post_type() ); ?>

					<div class="post-entry">
						<?php the_content( __( 'Read more &#8250;', 'responsive' ) ); ?>
						<?php wp_link_pages( array( 'before' => '<div class="pagination">' . __( 'Pages:', 'responsive' ), 'after' => '</div>' ) ); ?>
					</div><!-- end of .post-entry -->

					<?php responsive_entry_bottom(); ?>
				</div><!-- end of #post-<?php the_ID(); ?> -->
				<?php responsive_entry_after(); ?>

			<?php
			endwhile;

		endif; 	?>
	</div> <!-- end .grid .col-940 -->

<?php get_footer(); ?>



