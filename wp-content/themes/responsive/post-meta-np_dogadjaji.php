<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Post Meta-Data Template-Part File
 *
 * @file           post-meta.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/post-meta.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */
?>
<div class="post-meta">
    <?php if ( is_single() ): ?>
        <h1 class="entry-title post-title"><?php the_title(); ?></h1>
    <?php else: ?>
        <h2 class="entry-title post-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
    <?php endif; ?>

    <?php
        $datum = get_post_meta($post->ID, 'np_dogadjaji_datum_od', true);
        $vreme = get_post_meta($post->ID, 'np_dogadjaji_vreme_od', true);
        $organizacija = get_post_meta($post->ID, 'np_dogadjaji_naziv_organizacije', true);

        $grad = get_post_meta($post->ID, 'np_dogadjaji_grad', true);
        $ulica_broj = get_post_meta($post->ID, 'np_dogadjaji_ulica_broj', true);
        $prostor = get_post_meta($post->ID, 'np_dogadjaji_prostor', true);

        $opis = get_post_meta($post->ID, 'np_dogadjaji_opis', true);

        echo "<p><strong>Партнер: </strong>$organizacija</p>\r\n";
        echo "<strong>Време: </strong>" . $vreme ."\r\n";
        echo "<p><strong>Локација: </strong>$prostor, $ulica_broj, $grad</p>\r\n";
        echo "<p>$opis</p>\r\n";
    ?>
</div>