<?php

// Exit if accessed directly
if (!defined('ABSPATH')) {
	exit;
}

/**
 * Main Widget Template
 *
 *
 * @file           sidebar.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/sidebar.php
 * @link           http://codex.wordpress.org/Theme_Development#Widgets_.28sidebar.php.29
 * @since          available since Release 1.0
 */
?>

<style type="text/css">
	.relative{position: relative;}
	.cent-pos{
		top: 50%;
	    left: 50%;
	    transform: translate(-50%, -50%);
	    -webkit-transform: translate(-50%, -50%);
	    padding: 15px 25px;
		}
</style>

<?php responsive_widgets_before(); // above widgets container hook ?>
	<div id="widgets" class="grid col-300 fit"  >
		<?php responsive_widgets(); // above widgets hook ?>

		<?php if (!dynamic_sidebar('right-sidebar')): ?>
			<div class="widget-wrapper relative">
				<?php

					$ideje_tekst = get_field('ideja_tekst');
					$ideje_slika = get_field('ideja_slika');

					if ($ideje_slika) {
						$ideje_slika = $ideje_slika['url'];
					}

					if (empty($ideje_tekst)) {
						$ideje_tekst = "";
					}

				?>

				<img src="<?php echo $ideje_slika; ?>" class="img-responsive dark-img" >
				<div class="info-box-text cent-pos">
		            <?php echo $ideje_tekst; ?>
		        </div>
				<!-- get_stylesheet_directory_uri()/img/Box1.1.jpg -->
			<!-- 	<div class="widget-title"><h3><?php _e('In Archive', 'responsive');?></h3></div>
			<ul>
				<?php wp_get_archives(array('type' => 'monthly'));?>
			</ul> -->

			</div><!-- end of .widget-wrapper -->


			<div class="widget-wrapper" style="text-align: center;">

				<h2>Пратите нас</h2>
                <a href="https://www.facebook.com/nedeljaparlamentarizma?fref=ts" target="_blank"><img alt="" src="http://www.nedeljaparlamentarizma.rs/wp-content/uploads/2015/10/facebook.png"></a>
                <a href="https://twitter.com/ImateRec&#9;" target="_blank"><img alt="" src="http://www.nedeljaparlamentarizma.rs/wp-content/uploads/2015/10/twitter.png"></a>

			<!-- 	<div class="widget-title"><h3><?php _e('In Archive', 'responsive');?></h3></div>
			<ul>
				<?php wp_get_archives(array('type' => 'monthly'));?>
			</ul> -->

			</div><!-- end of .widget-wrapper -->
		<?php endif; //end of right-sidebar ?>

		<?php responsive_widgets_end(); // after widgets hook ?>
	</div><!-- end of #widgets -->
<?php responsive_widgets_after(); // after widgets container hook ?>