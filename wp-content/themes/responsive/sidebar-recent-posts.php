<?php responsive_widgets_before(); // above widgets container hook ?>
<div id="widgets" class="<?php echo implode(' ', responsive_get_sidebar_classes()); ?>">
<?php responsive_widgets(); // above widgets hook ?>

<?php
    $args = array(
        'numberposts' => '5',
        'post_type' => get_post_type(),
        'exclude' => get_the_ID()
    );
    $recent_posts = wp_get_recent_posts($args);
    if ( ! empty($recent_posts)) {
?>
    <div class="widget-wrapper">

        <div class="widget-title"><h3><?php _e('Остале вести', 'responsive'); ?></h3></div>
        <ul>

        <?php
            foreach ($recent_posts as $recent)
            {
            echo '<li><a href="' . get_permalink($recent["ID"]) . '">' . (__($recent["post_title"])) . '</a> <br/> <a>' . date('d.m.Y',
                strtotime($recent['post_date'])) . '</a> </li>';
            }
            ?>
        </ul>

    </div>
    <!-- end of .widget-wrapper -->
   <?php } ?>

    <?php responsive_widgets_end(); // after widgets hook
    ?>
    </div><!-- end of #widgets -->
    <?php responsive_widgets_after(); // after widgets container hook
    ?>