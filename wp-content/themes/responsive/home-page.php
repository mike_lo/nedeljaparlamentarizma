<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Home Page Template
 *
Template Name:  Home Page Template
 *
 */

/**
 * Globalize Theme Options
 */
$responsive_options = responsive_get_options();

    get_header();

    get_template_part( 'template-parts/featured-area' );

    get_footer();